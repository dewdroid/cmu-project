###################################################################################################

# 2
import matplotlib.pyplot as plt
spending_record = [[100,150,80,200,70,390,50,50,180,70,130,50,40,65,40],
                   [100,120,60,0,30,150,200,120,100,60,45,30,0,0,20], 
                   [430,0,0,100,0,0,200,120,350,0,0,0,100,0,35],
                   [0,25,40,100,120,100,80,0,20,10,200,10,40,250,50]]
types = ['food','transport','shopping','others']
plt .imshow( spending_record )
y = range( len(types) )
plt.yticks( y,types )
x1 = range( len(spending_record[0]) )
x2 = range( 1, len(spending_record[0]) + 1 )
plt.xticks(x1, x2)
plt.xlabel('Day')
plt.show()

###################################################################################################

# 3
import matplotlib.pyplot as plt
spending_record = [[100,150,80,200,70,390,50,50,180,70,130,50,40,65,40],
                   [100,120,60,0,30,150,200,120,100,60,45,30,0,0,20], 
                   [430,0,0,100,0,0,200,120,350,0,0,0,100,0,35],
                   [0,25,40,100,120,100,80,0,20,10,200,10,40,250,50]]
types = ['food','transport','shopping','others']
spending_by_type = [  sum(i)  for i in  spending_record  ]
y = range(  len(types)  )
plt.barh(  y, spending_by_type  )
plt.yticks(  y,types  )
plt.gca().invert_yaxis()
plt.xlabel('Spending (baht)')
plt.show()

###################################################################################################

# 4
from matplotlib.pyplot as plt
spending_record = [[100,150,80,200,70,390,50,50,180,70,130,50,40,65,40],
                   [100,120,60,0,30,150,200,120,100,60,45,30,0,0,20], 
                   [430,0,0,100,0,0,200,120,350,0,0,0,100,0,35],
                   [0,25,40,100,120,100,80,0,20,10,200,10,40,250,50]]
types = ['food','transport','shopping','others']
x = range(len( spending_record[0] ))
spending_by_day =  [] 
for i in x:
    sum = 0
    for j in range(len(spending_record)):
        sum += spending_record [j][i] 
    spending_by_day.append( sum )
for i in range( len(types) ):
    plt.plot(x, spending_record[i] , label= types[i] )
plt.plot(x, spending_by_day , label='total')
plt.legend()
x2 = range(1, len(spending_record[0]) + 1 )
plt.xticks(x,  x2 )
plt. ylabel ('Spending (baht)')
plt. xlabel ('Day')
plt. show ()

###################################################################################################

# 5
import matplotlib.pyplot as plt
spending_record = [[100,150,80,200,70,390,50,50,180,70,130,50,40,65,40],
                   [100,120,60,0,30,150,200,120,100,60,45,30,0,0,20], 
                   [430,0,0,100,0,0,200,120,350,0,0,0,100,0,35],
                   [0,25,40,100,120,100,80,0,20,10,200,10,40,250,50]]
types = ['food','transport','shopping','others']
spending_by_type = [  sum(i)  for i in  spending_record  ]
plt.pie ( spending_by_type , labels= types ,  autopct ='%.2f%%')
plt.show()