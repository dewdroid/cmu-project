# 2
def get_data():
    # Initialize container
    iris = {'data':[], 'labeltexts':[]}

    # Open file
    fid = open('iris.csv','r')   # Open a file   
    for (i,row) in enumerate(fid): # Loop each line of the file
        if i > 0:                # Skip first line because it is a header
            features = row.split(',')   # intermediate variable (ตัวแปรสร้างไว้แค่พักข้อมูล)
            iris['data'].append([float(features[1]), float(features[3])])  # Sepal width และ Petal width
            iris['labeltexts'].append(features[4].rstrip('\n'))

    # Close file
    fid.close()
    return iris

iris = get_data()
iris['labels'] = []
for i in iris['labeltexts']:
    if i == 'Iris-setosa':
        iris['labels'].append(0)
    elif i == 'Iris-versicolor':
        iris['labels'].append(1)
    else:
        iris['labels'].append(2)

# test
print('len =', len(iris['labels']))
print("iris['labels'] =", iris['labels'])

print("iris['labels'][2] =", iris['labels'][2])
print("iris['labels'][57] =", iris['labels'][57])
print("iris['labels'][103] =", iris['labels'][103])

###################################################################################################

# 3
def distances_from_unknown(unknown, data):
    distances = []
    for i in data:
        distance = ((unknown[0] - i[0]) ** 2 + (unknown[1] - i[1]) ** 2) ** 0.5
        distances.append(distance)
    return distances

# test
unknown = [1, 2]
data = [[2,1],[4,6],[3,4]]
print(distances_from_unknown(unknown, data))

unknown = [2, 2]
data = [[2,1],[4,6],[3,4]]
print(distances_from_unknown(unknown, data))

###################################################################################################

# 4
def get_k_nearest_labels(distances, labels, k):
    k_nearest = sorted(zip(distances, labels))    
    return [i[1] for i in k_nearest[:k]]

# test
distances = [ 2,  1,  3,  5,  4,  1]
labels    = ['a','b','c','d','e','f']
print(get_k_nearest_labels(distances, labels, 3))

distances = [ 2,  1,  3,  5,  4,  1]
labels    = ['a','b','c','d','e','f']
print(get_k_nearest_labels(distances, labels, 5))

distances = [ 100,  80,  45,  60,  4,  6]
labels    = [0, 1, 2, 3, 4, 5]
print(get_k_nearest_labels(distances, labels, 3))

###################################################################################################

# 5
def vote(top_k):
    if len(top_k) == 0:
        return 0
    return max(set(top_k), key=top_k.count)

print(vote([]))
print(vote([1,2,1]))
print(vote([1,1,1,2,2,2,2,2,2,2,0,0]))

###################################################################################################

# 6
def get_data():
    # Initialize container
    iris = {'data':[], 'labeltexts':[]}

    # Open file
    fid = open('iris.csv','r')   # Open a file   
    for (i,row) in enumerate(fid): # Loop each line of the file
        if i > 0:                # Skip first line because it is a header
            features = row.split(',')   # intermediate variable (ตัวแปรสร้างไว้แค่พักข้อมูล)
            iris['data'].append([float(features[1]), float(features[3])])  # Sepal width และ Petal width
            iris['labeltexts'].append(features[4].rstrip('\n'))

    # Close file
    fid.close()
    return iris

def distances_from_unknown(unknown, data):
    distances = []
    for i in data:
        distance = ((unknown[0] - i[0]) ** 2 + (unknown[1] - i[1]) ** 2) ** 0.5
        distances.append(distance)
    return distances

def get_k_nearest_labels(distances, labels, k):
    top_k = sorted(zip(distances, labels))    
    return [i[1] for i in top_k[:k]]

def vote(top_k):
    if len(top_k) == 0:
        return 0
    return max(set(top_k), key=top_k.count)

def predict(unknown, data, labels, k):
    distances = distances_from_unknown(unknown, data)
    top_k = get_k_nearest_labels(distances, labels, k)
    prediction = vote(top_k)
    return prediction
    
iris = get_data()

iris['labels'] = []
for i in iris['labeltexts']:
    if i == 'Iris-setosa':
        iris['labels'].append(0)
    elif i == 'Iris-versicolor':
        iris['labels'].append(1)
    else:
        iris['labels'].append(2)

# test
print('unknown = [3,1.6], k = 1 -->', predict([3,1.6], iris['data'], iris['labels'], 1) )
print('unknown = [3,1.6], k = 5 -->', predict([3,1.6], iris['data'], iris['labels'], 5) )
print('unknown = [3.5,0.5], k = 3 -->', predict([3.5,0.5], iris['data'], iris['labels'], 3) )