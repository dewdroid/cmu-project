# 1
def average(scoreList):
  total_sum = float(sum(scoreList))
  return total_sum/len(scoreList)

def standardDeviation(scoreList , mean):
    summation =  sum( [ (ele - mean)**2 for ele in scoreList] )
    sd = (summation/(len(scoreList)-1))**(0.5)
    return sd

def showAverageAndSd(studentList):
    scoreList = [student['attendance'] + student['midterm'] + student['final'] + sum(student['lab']) for student in studentList]
    mean = average(scoreList)
    sd = standardDeviation(scoreList , mean)
    print(round(mean, 2), round(sd, 2))


# 2
def showStandardGrades(studentList):
    for student in studentList:
        score = student['attendance'] + student['midterm'] + student['final'] + sum(student['lab'])
        if score >= 80:
            print(student['name'], 'A')
        elif score >= 70:
            print(student['name'], 'B')
        elif score >= 60:
            print(student['name'], 'C')
        elif score >= 50:
            print(student['name'], 'D')
        else:
            print(student['name'], 'F')


# 1, 2 test
studentList = [
    {"name": "Harry Potter", "attendance": 5, "midterm": 25, "final":34, "lab":[5,3,2,4]},
    {"name": "Hermione Granger", "attendance": 4, "midterm": 20, "final":30, "lab":[5,5,5,4]},
    {"name": "Ron Weasley", "attendance": 3, "midterm": 13, "final":25, "lab":[5,5,2,4]},
    {"name": "Darco Malfoy", "attendance": 0, "midterm": 30, "final":20, "lab":[3,3,5,2]},
    {"name": "Neville Longbottom", "attendance": 2, "midterm": 29, "final":16, "lab":[5,3,3,5]},
    {"name": "Peter Pettigrew", "attendance": 5, "midterm": 24, "final":29, "lab":[4,3,2,1]},
    {"name": "Ginny Weasley", "attendance": 4, "midterm": 18, "final":18, "lab":[2,5,2,5]},
    {"name": "Fred Weasley", "attendance": 3, "midterm": 16, "final":32, "lab":[4,3,2,4]},
    {"name": "George Weasley", "attendance": 3, "midterm": 29, "final":28, "lab":[4,4,4,4]},
    {"name": "Oliver Wood", "attendance": 4, "midterm": 27, "final":20, "lab":[4,5,5,5]}
]
showAverageAndSd(studentList)
showStandardGrades(studentList)