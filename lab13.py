import numpy as np
# 1
def equation(a, b, s):
    dataList = np.arange(a, b, s)
    return sum([(3 * (x ** 3)) + (2 * np.exp(1)) + (2 ** (2 * x + 1)) + ((x ** 2 + 1) ** 0.5) for x in dataList])

# test
ans = equation(0, 1, 0.001)
if not isinstance(ans, np.float64):
    raise ValueError('You have to use Numpy for this question')
print('{:.2f}'.format(ans))

###################################################################################################

# 4
def get_avgtemp_bymonth(data, month):
    sum_temp = 0
    n = 0
    for i in data:
        if i[2] == month:
            sum_temp += i[4]
            n += 1
    return sum_temp / n

def count_unique_loc_if(data, area_threshold):
    count_set = set()
    for i in data:
        if i[8] > area_threshold:
            count_set.add(f'{i[0]}{i[1]}')
    return len(count_set)

# test
data = np.genfromtxt('forestfire.csv', delimiter=',', skip_header=1)
print(get_avgtemp_bymonth(data, 1))
print(count_unique_loc_if(data, 10))
print("{:.2f}".format(get_avgtemp_bymonth(data, 2)))
print(count_unique_loc_if(data, 100))
print("{:.2f}".format(get_avgtemp_bymonth(data, 5)))
print(count_unique_loc_if(data, 200))

###################################################################################################